Lektor Bootstrap Theme
======================

.. highlights::
	A bootstrap theme for static websites that are generated with Lektor

:program:`lektor` is a static website generator.
:program:`bootstrap` is a css framework.

- lektor: https://www.getlektor.com
- bootstrap: https://getbootstrap.com
- gitlab: https://gitlab.com/ekleinod/lektor-bootstrap-theme
- examples
	- https://www.tt-schiri.de
