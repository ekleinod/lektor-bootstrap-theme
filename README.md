# Lektor Bootstrap Theme

Ein Theme für mit Lektor erzeugte, statische Webseiten.

- Dokumentation: <https://lektor-bootstrap-theme.readthedocs.io/en/latest/>
- gitlab: <https://gitlab.com/ekleinod/lektor-bootstrap-theme>
- Beispiele
	- <https://www.tt-schiri.de>
- Lektor: <https://www.getlektor.com/>

## Git-Repository

Die Webseite wird grundsätzlich über den main-Branch aktuell gehalten.
Programmierung wird über feature-Branches nach dem "stable mainline model" (http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/) organisiert.

Das heißt, der `main`-Branch enthält immer eine funktionierende Version.

## Rechtliches

Copyright 2020-2020 Ekkart Kleinod <schiri@ekkart>

Die Webseiten und jeglicher dazugehöriger Code bzw. Icons stehen, sofern sie selbst erstellt wurden, unter der Lizenz:

Creative-Commons-Lizenz Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International

http://creativecommons.org/licenses/by-nc-sa/4.0/deed.de

Laienhaft zusammengefasst: die selbsterstellten Dinge können verwendet werden, wenn die Autoren genannt werden und die Weitergabe ebenfalls unter gleicher Lizenz erfolgt.
Die Nutzung darf nicht kommerziell sein, ist eine kommerzielle Nutzung erwünscht, müssen die Autoren gefragt werden, die entscheiden ob und unter welchen Bedingungen eine kommerzielle Nutzung erlaubt ist.

Keine Nazis.
